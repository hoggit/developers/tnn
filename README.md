# TNN2

## Running pre-built versions

Open the built mission file (the `.miz`) from within DCS.
For a guide on how to interact with range spawning and JTACs, see
[here](https://wiki.hoggitworld.com/view/Hoggit_Training_Map).

### Development mode requirements/installation

Requires [H.O.G.G.I.T. Framework](http://framework.hoggitworld.com) to be loaded first.

For these instructions,
`USER_DIR` means your user-specific DCS directory, usually `C:\Users\You\Saved Games\DCS\` (or possibly `DCS.openbeta`)
and `GLOBAL_DIR` means your actual DCS install directory, e.g. `C:\DCS_World_OpenBeta`

- Add the [H.O.G.G.I.T. Framework](https://gitlab.com/hoggit/developers/hoggit) files inside a directory named `HOGGIT`
either as a copy or symlinking to a clone from git
(e.g. `USER_DIR\Scripts\HOGGIT\hoggit.lua` and `USER_DIR\Scripts\HOGGIT\hoggit\utils.lua`)
- Copy the `hoggit_config.lua.example` from the H.O.G.G.I.T. framework to your `GLOBAL_DIR\Scripts` directory (not `USER_DIR`)
- Edit the file and change the locations to have the paths of your `USER_DIR\Scripts` and `USER_DIR\Logs` directories.
- Add the TNN source to a `USER_DIR\Scripts\TNN` subdirectory
(again, either as a copy or a symlinked git clone)
- Comment out the `sanitizeModule` calls in `GLOBAL_DIR\Scripts\MissionScripting.lua`
(and remember to uncomment them when finished trying other frameworks and user scripts)

Running the mission from the mission editor will use any modifications to those script files.